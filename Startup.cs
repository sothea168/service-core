using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Service.CoreCenter.Utils;
using Service.CoreCenter.Utils.Database;
using Service.CoreCenter.Utils.Extension;
using Service.CoreCenter.Utils.RpcService;

namespace Service.CoreCenter
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            //Register rpc middleware
            services.AddGrpc(options =>
            {
                options.Interceptors.Add<RpcServiceInterceptor>();
            });
            services.AddScoped<INpgDbContext, NpgDbContext>(provider 
                => new NpgDbContext(_configuration.GetConnectionString("default")));

            services.AddScoped<IPreference, Utils.Database.Preference>();
            services.AddScoped<IRepository, Repository>();
            
            services.AddHttpContextAccessor();
            services.AddMemoryCache();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            app.UseRouting();
            _ = Task.Run(async () =>
            {
                await app.UsePostgresBroker();
            });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<PreferenceService>();
            });
        }
    }
}
