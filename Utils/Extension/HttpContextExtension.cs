﻿using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Service.CoreCenter.Utils.Extension
{
    public static class HttpContextExtension
    {
        public static TService GetService<TService>(this HttpContext context) =>
            context.RequestServices.GetServices<TService>().First();
    }
}
