﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Service.CoreCenter.Utils.Extension
{
    /// <summary>
    /// Additional function to string type
    /// </summary>
    public static class StringExtension
    {
        /// <summary>
        /// Compute plaint text to SHA512
        /// </summary>
        /// <param name="plain">Plaint text to convert</param>
        /// <returns></returns>
        public static string CalculateHash(this string plain)
        {
            using var sha = SHA256.Create();
            var bytes = Encoding.UTF8.GetBytes(plain);
            return BitConverter.ToString(sha.ComputeHash(bytes)).Replace("-", string.Empty);
        }
    }
}
