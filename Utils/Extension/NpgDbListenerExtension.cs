﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Service.CoreCenter.Utils.Database;

namespace Service.CoreCenter.Utils.Extension
{
    /// <summary>
    /// Listener extension
    /// </summary>
    public static class NpgDbListenerExtension
    {
        /// <summary>
        /// Start listen notify from npg
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static async Task UsePostgresBroker(this IApplicationBuilder app)
        {
            var broker = new NpgDbListener(app.ApplicationServices.GetService<IMemoryCache>());
            await broker.BrokerConfig(app.ApplicationServices.GetService<IConfiguration>().GetConnectionString("default"));
        }
    }
}