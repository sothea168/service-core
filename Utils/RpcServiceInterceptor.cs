﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Core.Interceptors;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Service.CoreCenter.Utils
{
    /// <summary>
    /// Rpc middleware <see cref="RpcServiceInterceptor"/>
    /// </summary>
    public class RpcServiceInterceptor: Interceptor
    {
        private readonly ILogger<RpcServiceInterceptor> _logger;
        private readonly IConfiguration _configuration;
        public RpcServiceInterceptor(ILogger<RpcServiceInterceptor> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TRequest"></typeparam>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <param name="continuation"></param>
        /// <returns></returns>
        public override async Task<TResponse> UnaryServerHandler<TRequest, TResponse>(
            TRequest request,
            ServerCallContext context,
            UnaryServerMethod<TRequest, TResponse> continuation)
        {
            try
            {
                if (!IsAuthenticated(context))
                    throw new RpcException(new Status(StatusCode.Unauthenticated, "Unauthenticated"));
                return await continuation(request, context);
            }
            catch (Exception ex)
            {
                // Note: The gRPC framework also logs exceptions thrown by handlers to .NET Core logging.
                _logger.LogError(ex, $"Error thrown by {context.Method}.");
                throw;
            }
        }
        bool IsAuthenticated(ServerCallContext context)
        {
            try
            {
                var cry = _configuration.GetSection("Credential").Get<string[]>();
                if (cry == null || cry.Any(c => c.Contains("*")))
                    return true;
                var ctx = context.GetHttpContext();

                return cry.Any(x => x == ctx.Request.Headers["Credential"].ToString());
            }
            catch (Exception e)
            {
                _logger.LogError(e, "error");
                throw;
            }
        }
    }
}
