﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Service.CoreCenter.Utils.Database.Entity;

namespace Service.CoreCenter.Utils.Database
{
    /// <summary>
    /// IPreference for predefine 
    /// </summary>
    public interface IPreference: IDisposable
    {
        Task<List<PreferenceDbResponse>> ListAsync(object request);
        Task<PreferenceDbResponse> SingleAsync(int id);
        Task<DbResponseBase<object>> UpdateAsync(object request);
    }
    /// <summary>
    /// Preference access model
    /// </summary>
    public class Preference: IPreference
    {
        private readonly INpgDbContext _dbContext;
        private readonly IMemoryCache _cache;
        /// <summary>
        /// Instantiate of <see cref="Preference"/> 
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="cache"></param>
        public Preference(INpgDbContext dbContext, IMemoryCache cache)
        {
            _dbContext = dbContext;
            _cache = cache;
        }
        /// <summary>
        /// Disposed all instance related.
        /// </summary>
        public void Dispose() => _dbContext.Dispose();
        /// <summary>
        /// Verify in memory cache
        /// </summary>
        /// <returns></returns>
        async Task<List<PreferenceDbResponse>> VerifyAsync()
        {
            if (_cache.TryGetValue(Constants.PoolKey, out SystemCacheCenter scc) && scc.Preferences != null)
                return scc.Preferences;
            scc ??= new SystemCacheCenter();
            _cache.Set(Constants.PoolKey, scc, TimeSpan.FromDays(30));
            return scc.Preferences = (await _dbContext.NpgFuncAsync<DbResponseBase<List<PreferenceDbResponse>>>("public.core_metadata_list", new { })).Data;
        }
        /// <summary>
        /// Fetch preference list <see cref="PreferenceListReply"/>
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<List<PreferenceDbResponse>> ListAsync(object request) => await VerifyAsync();
        /// <summary>
        /// Fetch preference single <see cref="PreferenceSingleReply"/>
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<PreferenceDbResponse> SingleAsync(int id) => (await VerifyAsync()).FirstOrDefault(x => x.Id == id);
        /// <summary>
        /// Modify preference 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<DbResponseBase<object>> UpdateAsync(object request) => await _dbContext.NpgFuncAsync<DbResponseBase<object>>("public.core_metadata_update", request);
    }
}
