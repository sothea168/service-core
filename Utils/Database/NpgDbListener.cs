﻿using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Npgsql;
using Service.CoreCenter.Utils.Database.Entity;

namespace Service.CoreCenter.Utils.Database
{
    /// <summary>
    /// Payload from npg notify
    /// </summary>
    internal class NotifyChangePayload
    {
        /// <summary>
        /// Table has been change
        /// </summary>
        public string Table { get; set; }
        /// <summary>
        /// Info changed 
        /// </summary>
        public string InfoChanged { get; set; }
    }

    /// <summary>
    /// Npg listener connection 
    /// </summary>
    public class NpgDbListener
    {
        private readonly IMemoryCache _cache;
        /// <summary>
        /// Instantiate listener instance
        /// </summary>
        /// <param name="cache"></param>
        public NpgDbListener(IMemoryCache cache) => _cache = cache;
        /// <summary>
        /// Perform notify change event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NotifyChange(object sender, NpgsqlNotificationEventArgs e)
        {
            var payload = JsonConvert.DeserializeObject<NotifyChangePayload>(e.Payload);
            if(payload == null 
               || string.IsNullOrEmpty(payload.Table) 
               || string.IsNullOrEmpty(payload.InfoChanged))return;
            if (!_cache.TryGetValue(Constants.PoolKey, out SystemCacheCenter scc))
            {
                return;
            }
            switch (payload.Table)
            {
                case "preference":
                {
                    if (scc.Preferences == null) return;
                    {
                        var info = JsonConvert.DeserializeObject<PreferenceDbResponse>(payload.InfoChanged);
                        if(info == null) return;
                        var single = scc.Preferences.FirstOrDefault(x => x.Id == info.Id);
                        if (single == null) return;

                        single.Value = info.Value;
                        single.Description = info.Description;
                    }
                    break;
                }
                case "metadata":
                {


                    break;
                }
            }
        }
        /// <summary>
        /// Start awaiting notify from database
        /// </summary>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public async Task BrokerConfig(string connectionString)
        {
            await using var connection = new NpgsqlConnection(connectionString);
            await connection.OpenAsync();
            connection.Notification += NotifyChange;
            await using var cmd = new NpgsqlCommand
            {
                CommandText = "LISTEN core_system_changed;", CommandType = CommandType.Text, Connection = connection
            };
            cmd.ExecuteNonQuery();
            while (true) await connection.WaitAsync();
        }
    }
}
