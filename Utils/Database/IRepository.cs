﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.CoreCenter.Utils.Database
{
    public interface IRepository: IDisposable
    {
        IPreference Preference { get; }
    }

    public class Repository: IRepository
    {
        public Repository(IPreference preference)
        {
            Preference = preference;
        }

        public IPreference Preference { get; }
        public void Dispose()
        {
            Preference.Dispose();
        }
    }
}
