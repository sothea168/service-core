﻿namespace Service.CoreCenter.Utils.Database.Entity
{
    public class PreferenceDbResponse
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
