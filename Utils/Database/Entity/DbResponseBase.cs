﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Service.CoreCenter.Utils.Database.Entity
{
    public abstract class DbResponseBase<TRsp>
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public TRsp Data { get; set; }
        public bool Success(Expression<Func<TRsp, bool>> predicate = null) => Code == NpgDbResponseCode.Ok && predicate == null;
    }
}
