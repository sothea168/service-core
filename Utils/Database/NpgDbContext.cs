﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Npgsql;

namespace Service.CoreCenter.Utils.Database
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class NpgConnect
    {
        private readonly string _connection;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connection"></param>
        protected NpgConnect(string connection) => _connection = connection;

        protected JsonSerializerSettings JsonSerializerSettings;
        private NpgsqlConnection _npgConnection;
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        protected async Task<T> Execute<T>(string sql)
        {
            await CreateConnectionAsync();
            await OpenConnectionAsync();
            await using var cmd = new NpgsqlCommand
            {
                CommandType = CommandType.Text,
                Connection = _npgConnection,
                CommandText = sql
            };

            try
            {
                var response = await cmd.ExecuteScalarAsync();
                return JsonConvert.DeserializeObject<T>(response?.ToString() ?? "{}", JsonSerializerSettings);
            }
            catch (Exception e)
            {
                return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(new { Code = "SYS-ERR", e.Message }), JsonSerializerSettings);
            }
        }
        /// <summary>
        /// Create postgres connection
        /// </summary>
        /// <returns></returns>
        private async Task CreateConnectionAsync()
        {
            _npgConnection = new NpgsqlConnection(_connection);
            await Task.CompletedTask;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private async Task OpenConnectionAsync() => await _npgConnection.OpenAsync();
    }
    /// <summary>
    /// 
    /// </summary>
    public interface INpgDbContext: IDisposable
    {
        Task<T> NpgFuncAsync<T>(string sql, object param);
    }
    /// <summary>
    /// 
    /// </summary>
    public class NpgDbContext: NpgConnect, INpgDbContext
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connection"></param>
        public NpgDbContext(string connection): base(connection)
            => JsonSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver { NamingStrategy = new SnakeCaseNamingStrategy() }
            };

        /// <summary>
        /// Execute postgres sql function
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql">Postgres function name</param>
        /// <param name="param">request as json object</param>
        /// <returns>Json object:= new{ code = "OK", message = "Accepted", data = {} }</returns>
        public async Task<T> NpgFuncAsync<T>(string sql, object param)
            => await Execute<T>($"SELECT {sql}('{JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param, JsonSerializerSettings))}')");

        /// <summary>
        /// Dispose npg database connection
        /// </summary>
        public void Dispose() { }
    }
}
