﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Service.CoreCenter.Utils.Database;
using Service.CoreCenter.Utils.Extension;

namespace Service.CoreCenter.Utils.RpcService
{
    public class PreferenceService: Preference.PreferenceBase, IDisposable
    {
        private readonly IRepository _repository;
        public PreferenceService(HttpContext context) => _repository = context.GetService<IRepository>();

        public override async Task<PreferenceListReply> List(PreferenceListRequest request, ServerCallContext context)
        {
            var list = await _repository.Preference.ListAsync(request);
            var cursor = list.Where(cur => string.IsNullOrEmpty(request.Query) || cur.Value.Contains(request.Query) || cur.Description.Contains(request.Query)).Select(x => new PreferenceListReply.Types.PreferenceObject
            {
                Id = x.Id, Key = x.Key, Value = x.Value, Description = x.Description
            });
            return new PreferenceListReply
            {
                Code = NpgDbResponseCode.Ok,
                Message = "Accepted",
                Data = new PreferenceListReply.Types.DataReply { Records = { JsonConvert.DeserializeObject<List<PreferenceListReply.Types.PreferenceObject>>(JsonConvert.SerializeObject(cursor)) } }
            };
        }

        public override async Task<PreferenceSingleReply> Single(PreferenceSingleRequest request, ServerCallContext context)
        {
            var single = await _repository.Preference.SingleAsync(request.Id);
            return new PreferenceSingleReply
            {
                Code = NpgDbResponseCode.Ok,
                Message = "Accepted",
                Data = new PreferenceSingleReply.Types.DataReply
                {
                    Description = single.Description, Id = single.Id, Value = single.Value, Key = single.Key
                }
            };
        }

        public override async Task<PreferenceUpdateReply> Update(PreferenceUpdateRequest request, ServerCallContext context)
        {
            var update = await _repository.Preference.UpdateAsync(request);
            return new PreferenceUpdateReply
            {
                Code = update.Code,
                Message = update.Message
            };
        }
        
        public void Dispose() => _repository.Dispose();
    }
}
