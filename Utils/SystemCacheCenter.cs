﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Service.CoreCenter.Utils.Database.Entity;

namespace Service.CoreCenter.Utils
{
    public class SystemCacheCenter
    {
        public List<PreferenceDbResponse> Preferences { get; set; }

    }
}
